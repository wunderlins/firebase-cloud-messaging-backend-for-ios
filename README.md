# Firebase Cloud Message Test UI

Source: [https://gitlab.com/wunderlins/firebase-cloud-messaging-backend-for-ios](https://gitlab.com/wunderlins/firebase-cloud-messaging-backend-for-ios)

## About
This is a minimalistic implementation to send Push notification to iOS via Firebase Cloud Messaging via [V1 FCM API. ](https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages). The message formate that firebase sends to the apple service does not seem to be well documented. This project can be used as starting point. A working FCM Configuration is required (Project setup, valid [APN Keys](https://firebase.google.com/docs/cloud-messaging/ios/certs) and a working [iOS Client App](https://firebase.google.com/docs/cloud-messaging/ios/client)). The project uses the Admin SDK to deliver messages.

After starting the application, a web interface can be found at [http://localhost:8080](http://localhost:8080). There is a swagger Interface to be found at [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/).

## Prepare

* Download Service account key (Firebase Console » Project » Project Settings » Service Accounts)
* store it under `src/main/resources/firebase-adminsdk.json` or if you are running it as a jar, copy the key into the same folder as the jar and name it `firebase-adminsdk.json`
* check if maven is installed

## Run

* run with `./run.sh` or if there is no bash, alternatively:
* `maven spring-boot:run`
* open web-interface under [http://localhost:8080/](http://localhost:8080/)
* Start your iOS application and log the FCMToken of the physical device (will not work in simulator)
* copy `Firebase registration token:` from console
* You need to enter the FCMToken into the web interface before sending messages


## application.properties

If you wish to use a different port of path name for the firebase Admin SDK file you 
may configure this with an `application.properties` file which has to be placed next to 
the jar file.

Example file:

```
server.port=8080
firebase.admin_sdk_file=firebase-adminsdk.json
```


## Build

* `package.sh`
* jar file can be found under `target/`
* copy jar file to any location. 
* copy your FCM Admin Key into the same folder and name it `firebase-adminsdk.json`
* directory structure should now look like this:
    * FCMTest-NNN.jar
    * firebase-adminsdk.json
* run `java -jar FCMTest-NNN.jar` form the same folder as the jar

## Links

General REST information for Spring-Boot:
https://www.concretepage.com/spring-boot/spring-boot-rest-example#Maven

Firebase FCM API:
https://firebase.google.com/docs/reference/admin/java/reference/com/google/firebase/messaging/package-summary?authuser=0

Spring-Boot FCM Example:
https://blog.mestwin.net/send-push-notifications-from-spring-boot-server-side-application-using-fcm/

Notification Content for iOS:
https://stackoverflow.com/questions/38166203/ios-data-notifications-with-fcm

Firebase APS Payload for iOS:
https://firebase.google.com/docs/cloud-messaging/ios/receive#handle-payload

Message formats for Android:
https://firebase.google.com/docs/cloud-messaging/send-message#example-notification-message-with-platform-specific-delivery-options