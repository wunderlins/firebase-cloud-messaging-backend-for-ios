#!/usr/bin/env bash

mvn package

# TODO: excluding in pom
remove=`unzip -l target/FCMTest-0.0.1-SNAPSHOT.jar | grep "firebase-adminsdk.json" | awk '{print $4}'`
for f in $remove; do
    zip -d target/FCMTest-0.0.1-SNAPSHOT.jar $f
done

#remove=`unzip -l target/FCMTest-0.0.1-SNAPSHOT.jar | grep "application.properties" | awk '{print $4}'`
#for f in $remove; do
#    zip -d target/FCMTest-0.0.1-SNAPSHOT.jar $f
#done
