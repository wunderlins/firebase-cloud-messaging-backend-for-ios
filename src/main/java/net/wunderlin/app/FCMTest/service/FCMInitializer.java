package net.wunderlin.app.FCMTest.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import net.wunderlin.app.FCMTest.FcmTestApplication;

@Service
public class FCMInitializer implements ApplicationContextAware {

    private ApplicationContext context;

    @Value("${firebase.admin_sdk_file}")
    private String firebaseConfigPath;

    Logger logger = LoggerFactory.getLogger(FCMInitializer.class);

    public void shutdownContext() {
        ((ConfigurableApplicationContext) context).close();
    }
    
    @PostConstruct
    public void initialize() {

        logger.info("Base directory: " + FcmTestApplication.getBaseDirectory());
        InputStream serviceAccount = null;

        // find admin SDK Config file
        File f = null;
        // we check:
        // - first try if the path resolves
        f = new File(firebaseConfigPath);
        logger.info("Checked: " + f.toString());

        // - if not, check in the classpath/resources directory
        if (!f.exists()) {
            Path currentPath = FcmTestApplication.getBaseDirectory().toPath();
            Path filePath = Paths.get(currentPath.toString(), firebaseConfigPath);
            f = filePath.toFile();
            //logger.info("Checked: " + f.toString());
            // if (f.exists()) {
            // logger.info("Found Admin SDK config: " + f.toString());
            // }
        }

        // - try jar directory/config
        if (!f.exists()) {
            Path currentPath = FcmTestApplication.getBaseDirectory().toPath();
            Path filePath = Paths.get(currentPath.toString(), "config", firebaseConfigPath);
            f = filePath.toFile();
            //logger.info("Checked: " + f.toString());
            // if (f.exists()) {
            // logger.info("Found Admin SDK config: " + f.toString());
            // }
        }

        // - if not check in resources
        if (!f.exists()) {
            ClassPathResource r = new ClassPathResource(firebaseConfigPath);
            try {
                //logger.info("Checked:" + r.getFile().toString());
                f = r.getFile();
                /*
                if (f.exists()) {
                    logger.info("Found Admin SDK config: " + f.toString());
                }
                */
            } catch (IOException e1) {
                logger.error("Admin SDK Config file not found");
                shutdownContext();
                return;
            }
        }

        logger.info("FCM Admin SDK Config File: " + f.toString());
        try {
            serviceAccount = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.error("Could not open Admin SDK Config file. Copy it into the same folder as your jar and name it " + firebaseConfigPath);
            shutdownContext();
        }

        // initialize Firebase
        FirebaseOptions options = null;
        try {
            options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (FirebaseApp.getApps().isEmpty()) {
            FirebaseApp.initializeApp(options);
            logger.info("Firebase application has been initialized");
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

}