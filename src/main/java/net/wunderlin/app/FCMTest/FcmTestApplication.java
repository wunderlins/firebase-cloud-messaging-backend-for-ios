package net.wunderlin.app.FCMTest;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;

@SpringBootApplication
public class FcmTestApplication {

	static Logger logger = LoggerFactory.getLogger(FcmTestApplication.class);

	public static File getBaseDirectory() {
		ApplicationHome home = new ApplicationHome(FcmTestApplication.class);
		return home.getDir();    // returns the folder where the jar is. This is what I wanted.
	}

	public static void main(String[] args) {
		SpringApplication.run(FcmTestApplication.class, args);
	}

}
