package net.wunderlin.app.FCMTest.controller;

import org.springframework.web.bind.annotation.RequestMapping;

public class IndexPageController {
    @RequestMapping("/")
    public String index() {
        return "index.html";
    }
}

