package net.wunderlin.app.FCMTest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.Notification.Builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class FirebaseCloudMessagesController {

  @Value("${firebase.admin_sdk_file}")
  private String firebaseConfigPath;

  Logger logger = LoggerFactory.getLogger(FirebaseCloudMessagesController.class);

  public FirebaseCloudMessagesController() {}

  
  @ApiOperation(value = "Send Notification to FCM Service")
  @ApiResponses(value = { 
          @ApiResponse(code = 200, message = "Suceess|OK"),
          @ApiResponse(code = 500, message = "FCM Error Response")
  })

  @PostMapping(
    path = "/api/v1/sendMessageToDevice", 
    consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
    produces = "application/json"
  )
  public ResponseEntity<String> sendMessageToDevice(
    @RequestParam("title") String title,
    @RequestParam("body") String body, 
    @RequestParam("fcmtoken") String fcmtoken,
    @RequestParam("badge") Integer badge // actually Int
  ) {

    logger.info("Title: " + title);
    logger.info("Body: "  + body);
    logger.info("Badge: " + badge); // Int
    logger.info("Token: " + fcmtoken);

    // for iOS to display Message when app is in background, 
    // use the following structure. This is how apple wants it
    //
    // {  
    //   "notification": {
    //     "body" : "This week’s edition is now available.",
    //     "title": "Portugal vs. Denmark",
    //     "text": "5 to 1",
    //     "content_available": 1
    //     },
    //   "priority" : "high",
    //   "badge": "nnn"
    // }
    //
    // https://firebase.google.com/docs/cloud-messaging/ios/receive
    // This is how Firebase wants it to produce the above.
    // {
    //   "aps" : {
    //     "alert" : {
    //       "body" : "great match!",
    //       "title" : "Portugal vs. Denmark",
    //     },
    //     "sound": "default",
    //     "badge" : 1,
    //   },
    //   "priority": "high",
    // }
    
    Builder notification = Notification.builder();
    notification.setTitle(title);
    notification.setBody(body);

    ApnsConfig.Builder apnsConfig = ApnsConfig.builder();
    Aps.Builder aps = Aps.builder();
    aps.setBadge(badge);
    aps.setSound("default");
    apnsConfig.setAps(aps.build());
    
    // See documentation on defining a message payload.
    Message message = Message.builder()
      .setApnsConfig(apnsConfig.build())
      .setNotification(notification.build())
      .putData("priority", "high")
      .setToken(fcmtoken)
      .build();
    logger.info("Message: " + message.toString());

    // Send a message to the device corresponding to the provided
    // registration token.
    String response = "";
    try {
      response = FirebaseMessaging.getInstance().send(message);
    } catch (FirebaseMessagingException e) {
      // TODO Auto-generated catch block
      System.out.println(e);
      e.printStackTrace();
      return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY)
        .body(e.toString());
    }
    // Response is a message ID string.
    System.out.println("Successfully sent message: " + response);
    
    return ResponseEntity.ok(response);
  }

}